    //stdin-stdout
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

    //fungsi input keyboard
function input(question){
    return new Promise(resolve => {
        readline.question(question, data=>{
            return resolve(data);
        });
    });
}
    //fungsi main
async function main(){
    let stop = false;
    do{
        let kumpulanNilai = [];
        let berhenti = false;
        let countLulus = 0;
        let countGagal = 0;
        let jumlah = 0;
        let average = 0;
        let i = 1;
        console.log("");
        console.log("PROGRAM NILAI");
        console.log("*Program berhenti jika memasukkan value selain angka*");

    //input nilai + hitung lulus/gagal + hitung rata-rata
        do{
            nilai = await input(`Input nilai ${i} = `);
            if(isNaN(nilai)){
                average = jumlah/(i-1);
                berhenti = true;
            } else {
                jumlah += +nilai;
                kumpulanNilai.push(+nilai);
                i++;
                if(nilai>=60){
                    countLulus++;
                } else {
                    countGagal++;
                }
            }
        } while(!berhenti);

    //sorting (insertion sort)
        for(let i = 1; i<kumpulanNilai.length; i++){
            let element = kumpulanNilai[i];
            let j = i-1;
            while(kumpulanNilai[j]>element && j>=0){
                kumpulanNilai[j+1]=kumpulanNilai[j];
                j--;
            }
            kumpulanNilai[j+1] = element;
        }

    //tampilkan semua nilai yang dibutuhkan ke terminal
        console.log(``);
        console.log(`Nilai tertinggi : ${kumpulanNilai[kumpulanNilai.length-1]}`);
        console.log(`Nilai terendah : ${kumpulanNilai[0]}`);
        console.log(`Rata-rata : ${average}`);
        console.log(`Siswa Lulus : ${countLulus}, Siswa Tidak Lulus : ${countGagal}`);
        console.log(`Urutan nilai dari terendah ke tertinggi : ${kumpulanNilai}`);
        console.log(`Nilai 90 : ${kumpulanNilai.filter(value=>value==90)}`);
        console.log(`Nilai 100 : ${kumpulanNilai.filter(value=>value==100)}`);

    //ingin menggunakan lagi atau tidak
        console.log(``);
        let menu = await input(`Ingin menggunakan program lagi = (y/n) `);
        if(menu=="y"){
            stop = false;
        } else if(menu=="n"){
            readline.close();
            stop = true;
        } else {
            console.log(`Pilihan salah, program berhenti`);
            readline.close();
            stop = true;
        }
    }while(!stop);
}

    //panggil fungsi main
main();